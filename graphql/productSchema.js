const GraphQLSchema = require('graphql').GraphQLSchema;
const GraphQLObjectType = require('graphql').GraphQLObjectType;
const GraphQLList = require('graphql').GraphQLList;
const GraphQLNonNull = require('graphql').GraphQLNonNull;
const GraphQLID = require('graphql').GraphQLID;
const GraphQLString = require('graphql').GraphQLString;
const GraphQLInt = require('graphql').GraphQLInt;
const ProductModel = require('../models/product');

const productType = new GraphQLObjectType({
    name: 'product',
    fields: function () {
      return {
        _id: {
          type: GraphQLString
        },
        name: {
          type: GraphQLString
        },
        price: {
          type: GraphQLInt
        }
      }
    }
});

const queryType = new GraphQLObjectType({
    name: 'Query',
    fields: function () {
      return {
        products: {
          type: new GraphQLList(productType),
          resolve: function () {
            const products = ProductModel.find({});
            if (!products) {
              throw new Error('Error');
            }
            return products;
          }
        },
        product: {
          type: productType,
          args: {
            id: {
              name: '_id',
              type: GraphQLString
            }
          },
          resolve: function (root, params) {
            const productDetails = ProductModel.findById(params.id);
            if (!productDetails) {
              throw new Error('Error');
            }
            return productDetails;
          }
        }
      }
    }
});

const mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: function () {
      return {
        addProduct: {
          type: productType,
          args: {
            name: {
              type: new GraphQLNonNull(GraphQLString)
            },
            price: {
              type: new GraphQLNonNull(GraphQLInt)
            }
          },
          resolve: function (root, params) {
            const productModel = new ProductModel(params);
            const newProduct = productModel.save();
            if (!newProduct) {
              throw new Error('Error');
            }
            return newProduct;
          }
        },
        updateProduct: {
          type: productType,
          args: {
            id: {
              name: 'id',
              type: new GraphQLNonNull(GraphQLString)
            },
            name: {
              type: new GraphQLNonNull(GraphQLString)
            },
            price: {
              type: new GraphQLNonNull(GraphQLInt)
            }
          },
          resolve(root, params) {
            return ProductModel.findByIdAndUpdate(params.id, { name: params.name, price: params.price }, function (err) {
              if (err) return next(err);
            });
          }
        },
        removeProduct: {
          type: productType,
          args: {
            id: {
              type: new GraphQLNonNull(GraphQLString)
            }
          },
          resolve(root, params) {
            const remProduct = ProductModel.findByIdAndRemove(params.id);
            if (!remProduct) {
              throw new Error('Error');
            }
            return remProduct;
          }
        }
      }
    }
});

module.exports = new GraphQLSchema({query: queryType, mutation: mutation});