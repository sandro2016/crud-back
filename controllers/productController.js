const Product = require("../models/product");

exports.list = async(request, response) => {
    try {
        result = await Product.find({});

        return response.status(200).json(result);
    } catch (error) {
        console.log(error);

        return response.status(400).json(error);
    }
}

exports.create = async (request, response) => {
    try {
        if(!request.body) return response.status(400).json({error: 'Empty request'});

        const name = request.body.name;
        const price = request.body.price;
    
        await Product.create({name: name, price: price});
    
        return response.status(201).json(null);
    } catch (error) {
        console.log(error);
        
        return response.status(400).json(error);
    }
}

exports.get = async (request, response) => {
    try {
        const id = request.params["id"];

        result = await Product.findById(id);

        return response.status(200).json(result);
    } catch(error) {
        console.log(error);

        return response.status(400).json(error);
    }
}

exports.update = async (request, response) => {
    try {
        if(!request.body) return response.status(400).json({error: 'Empty request'});

        const id = request.body._id;
        const name = request.body.name;
        const price = request.body.price;

        await Product.findByIdAndUpdate(id, {name: name, price: price});

        return response.status(204).json(null);
    } catch (error) {
        console.log(error);

        return response.status(400).json(error);
    }
}

exports.delete = async (request, response) => {
    try {
        const id = request.params["id"];

        await Product.findByIdAndDelete(id);

        return response.status(204).json(null);
    } catch(error) {
        console.log(error);

        return response.status(400).json(error);
    }
}