const express = require("express");
const productController = require("../controllers/productController");

const productRouter = express.Router();

productRouter.post("/create", productController.create);
productRouter.put("/update", productController.update);
productRouter.delete("/delete/:id", productController.delete);
productRouter.get("/:id", productController.get);
productRouter.get("/", productController.list);

module.exports = productRouter;