const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const productRouter = require("./routes/productRouter");
const logService = require("./services/logService");
const graphqlHTTP = require("express-graphql");
const schema = require("./graphql/productSchema");

const app = express();

app.use(cors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "headers": "Content-Type"
}));

app.use(function(request, response, next){
    logService.log(request);
    next();
});

app.use('/graphql', cors(), graphqlHTTP({
    schema: schema,
    rootValue: global,
    graphiql: true
}));

app.use(bodyParser.json());

app.use("/product", productRouter);

app.use(function(request, response, next){
    response.status(404).json(null);
});

mongoose.connect("mongodb://localhost:27017/products", {useNewUrlParser: true}, function(err) {
    if(err) return console.log(err);
    app.listen(3000, function(){
        console.log("Server is running..");
    });
});